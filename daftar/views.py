from django.shortcuts import render
from django.http import HttpResponse
import requests

# Create your views here.
def index(req):
    return HttpResponse("Welcome to Index")

def register(req):
    return HttpResponse("Please register your TA")

def regseminar(req):
    return HttpResponse("Seminar registration")

def seminar(req):
    return HttpResponse("Checklist pelaksanaan seminar")

def beritaacara(req):
    return HttpResponse("Berita acara")

def revisiseminar(req):
    return HttpResponse("Revisi seminar")

def nilaiseminar(req):
    return HttpResponse("Nilai seminar dari dosen pembimbing dan penguji")

#halaman insentif seminar
#halaman pengaturan pertanyaan
#halaman daftar hadir mahasiswa
