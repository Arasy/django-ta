from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Prodi)
admin.site.register(Peminatan)
admin.site.register(TopikTA)
admin.site.register(Mahasiswa)
admin.site.register(Dosen)
admin.site.register(JudulTA)
admin.site.register(Seminar)
admin.site.register(TugasAkhir)

