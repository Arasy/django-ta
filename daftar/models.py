from django.db import models

# Create your models here.
class Prodi(models.Model):
    kode_prodi = models.CharField(max_length=2)
    nama_prodi = models.CharField(max_length=20)
    def __str__(self):
        return self.nama_prodi

class Peminatan(models.Model):
    program_studi = models.ForeignKey(Prodi,on_delete=models.CASCADE)
    nama_peminatan = models.CharField(max_length=30)
    def __str__(self):
        return "%s : %s" %(self.program_studi,self.nama_peminatan)

class Mahasiswa(models.Model):
    NIM = models.CharField(max_length = 15)
    Nama = models.CharField(max_length = 50)
    program_studi = models.ForeignKey(Prodi, on_delete=models.CASCADE)
    peminatan = models.ForeignKey(Peminatan, on_delete=models.CASCADE)
    def __str__(self):
        return self.Nama

class Dosen(models.Model):
    Nama = models.CharField(max_length = 50)
    gelar_depan = models.CharField(max_length = 10)
    gelar_belakang = models.CharField(max_length = 20)
    aktif = models.BooleanField(default=True)
    def __str__(self):
        if(self.gelar_depan):
            if(self.gelar_belakang):
                return "%s %s, %s" % (self.gelar_depan,self.Nama,self.gelar_belakang)
            else:
                return "%s %s" % (self.gelar_depan,self.Nama)
        else :
            if(self.gelar_belakang):
                return "%s, %s" % (self.Nama, self.gelar_belakang)
            else:
                return self.Nama

class TopikTA(models.Model):
    kategori_topik = models.CharField(max_length=20)
    def __str__(self):
        return self.kategori_topik

class JudulTA(models.Model):
    pengusul = models.ForeignKey(Dosen,on_delete=models.CASCADE)
    topik = models.ForeignKey(TopikTA,on_delete=models.CASCADE)
    judul = models.CharField(max_length=100)
    def __str__(self):
        return self.judul

class Ruangan(models.Model):
    nama = models.CharField(max_length=10)
    deskripsi = models.CharField(max_length=100)

class TugasAkhir(models.Model):
    JenisTA = [
        ("TA 1", "Tugas Akhir 1"),
        ("TA 2", "Tugas Akhir 2"),
    ]

    id_mhs = models.ForeignKey(Mahasiswa,on_delete=models.CASCADE)
    dosen_pembimbing = models.ForeignKey(Dosen,on_delete=models.CASCADE)
    kategori = models.CharField(max_length=5,choices=JenisTA,default="TA 1")
    judul = models.ForeignKey(JudulTA,on_delete=models.CASCADE)
    tgl_pengajuan = models.DateTimeField("tanggal pengajuan", auto_now_add=True)
    approval_dosen = models.BooleanField()
    tgl_approval = models.DateTimeField()
    approval_kaprodi = models.BooleanField()
    tgl_approval_kaprodi = models.DateTimeField()
    semester_pengajuan = models.CharField(max_length=10)

class Seminar(models.Model):
    JenisSeminar = [
        ("Sem.Prop","Seminar Proposal"),
        ("Sem.Has","Seminar Hasil"),
        ("Sidang","Sidang Akhir"),
    ]

    tgl_pengajuan_seminar = models.DateTimeField(null=True, blank=True)
    tgl_seminar = models.DateTimeField()
    seminar = models.CharField(max_length=10,choices=JenisSeminar,default="Sem.Prop")
    TA = models.ForeignKey(TugasAkhir,on_delete=models.CASCADE)
    ruang = models.ForeignKey(Ruangan,on_delete=models.CASCADE)

class InsentifSeminar(models.Model):
    id_insentif = models.CharField(max_length=10) #tahun akademik pengajuan + tahap
    tgl_pengajuan = models.DateTimeField()
    tgl_pembayaran = models.DateTimeField()

class PengujiSeminar(models.Model):
    seminar = models.ForeignKey(Seminar,on_delete=models.CASCADE)
    dosen_penguji = models.ForeignKey(Dosen,on_delete=models.CASCADE)
    nilai = models.FloatField(null=True)
    pending_nilai = models.FloatField(default = False)
    rekap = models.ForeignKey(InsentifSeminar,on_delete=models.CASCADE)

class PembimbingSeminar(models.Model):
    seminar = models.ForeignKey(Seminar, on_delete=models.CASCADE)
    dosen_pembimbing = models.ForeignKey(Dosen, on_delete=models.CASCADE)
    nilai = models.FloatField(null=True)
    pending_nilai = models.FloatField(default = False)
    rekap = models.ForeignKey(InsentifSeminar,on_delete=models.CASCADE)

class PenontonSeminar(models.Model):
    seminar = models.ForeignKey(Seminar, on_delete=models.CASCADE)
    mahasiswa = models.ForeignKey(Mahasiswa,on_delete=models.CASCADE)

